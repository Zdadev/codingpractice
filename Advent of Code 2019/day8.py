def split_into_layers(pic_string, x_dim, y_dim):
    layers = []
    i = 0
    size = x_dim*y_dim
    while i < len(pic_string):
        layers.append(pic_string[i:(i+size)])
        i += size
    return layers

input_file = open("./inputs/day8input.txt", "r")
x_dim = 25
y_dim = 6
file_string = input_file.read()
layers = split_into_layers(file_string, x_dim, y_dim)

#part1
min_zero = x_dim*y_dim+1
min_layer = None
for i in layers:
    zeros = i.count('0')
    if zeros < min_zero:
        min_zero = zeros
        min_layer = i
print(min_layer.count('1')*min_layer.count('2'))

#part2
answer_string = ""
for i in range(0, x_dim*y_dim):
    j = 0
    color = '2'
    while(color == '2' and j < len(layers)):
        color = layers[j][i]
        j+=1
    answer_string += color

i = 0
while(i<len(answer_string)):
    print(answer_string[i:i+25])
    i+=25
print(answer_string)