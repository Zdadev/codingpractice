#https://adventofcode.com/2019/day/4
def brute_force_passwords(start, end):
    count = 0
    for i in range(start, end+1):
        stringified = str(i)
        fits_criteria = False
        repeat_count = 1
        contains_double = False
        for j in range(1, len(stringified)):
            if stringified[j] == stringified[j-1]:
                fits_criteria = True
                repeat_count += 1
                if repeat_count > 2:
                    fits_criteria = False
            else:
                if repeat_count == 2:
                    contains_double = True
                repeat_count = 1
        if contains_double:
            fits_criteria = True

        if fits_criteria:
            for j in range(1, len(stringified)):
                if not stringified[j] >= stringified[j-1]:
                    fits_criteria = False

        if fits_criteria:
            print(i)
            count+=1
    return count

print(brute_force_passwords(147981, 691423))