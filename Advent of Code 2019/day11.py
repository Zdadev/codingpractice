from day9Intcode import IntCode
from enum import Enum

def get_input(file_loc):
    input_file = open(file_loc, "r")
    # input_file = open("day7.test.txt", "r")
    split_array = input_file.read().split(",")
    for i in range(0, len(split_array)):
        split_array[i] = int(split_array[i])
    return split_array

def get_color(loc_string, painted, default=0):
    if loc_string in painted:
        return painted[loc_string]
    return default

def loc_tostring(loc):
    return (str(loc[0]) + ","+ str(loc[1]))

direction_enum = [[0,-1],[1, 0],[0, 1],[-1, 0]]
facing = 0
location = [0,0]

painted = {}

input_array = get_input("./inputs/day11input.txt")
# robot = IntCode(input_array, [0]) part 1
robot = IntCode(input_array, [1])

while(True):
    return_val = robot.run_intcode()
    if(return_val != 1):
        print(return_val)
        break
    paint_color = robot.output[0]
    turn = robot.output[1]
    painted[loc_tostring(location)] = paint_color
    
    if(turn == 0):
        facing = facing - 1
    else:
        facing = facing + 1
    
    location[0] += direction_enum[facing%4][0]
    location[1] += direction_enum[facing%4][1]
    robot.add_input([get_color(loc_tostring(location), painted)])
#part 2
#turn keys into arrays
max_x = 0
max_y = 0
min_x = 0
min_y = 0


for key in painted.keys():
    split_key = key.split(',')
    x = int(split_key[0])
    y = int(split_key[1])
    max_x = max(x, max_x)
    max_y = max(y, max_y)
    min_x = min(x, min_x)
    min_y = min(y, min_y)

grid = [" "]*(max_y-min_y+1)
for i in range(0, max_y-min_y+1):
    grid[i] = [" "]*(max_x-min_x + 1)

for key in painted.keys():
    split_key = key.split(',')
    x = int(split_key[0]) - min_x
    y = int(split_key[1]) - min_y
    grid[y][x] = "#" if painted[key] == 1 else " "

print(max_x-min_x, max_y-min_y)
for row in grid:
    print(" ".join(row))

