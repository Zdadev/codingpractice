#This is an unoptimized mess, but I had already done three exercises and just wanted to get it over with
#Maybe I'll revisit this and clean it up
import time
from day9Intcode import IntCode
from Grid import Grid


def get_input(file_loc):
    input_file = open(file_loc, "r")
    # input_file = open("day7.test.txt", "r")
    split_array = input_file.read().split(",")
    for i in range(0, len(split_array)):
        split_array[i] = int(split_array[i])
    return split_array

tile_id = {
    0:" ",
    1:"H",
    2:".",
    3:"_",
    4:"o"
}
#part 1

input_array = get_input("./inputs/day13input.txt")
cabinet1 = IntCode(input_array, [])
cabinet1.run_intcode()

i = 0
tiles = [None]*(len(cabinet1.output)//3)
while i < len(cabinet1.output):
    tiles[i//3]=[cabinet1.output[i], cabinet1.output[i+1], cabinet1.output[i+2]]
    i+=3

max_x = 0
max_y = 0
for tile in tiles:
    max_x = max(tile[0], max_x)
    max_y = max(tile[1], max_y)

game_grid1 = Grid(max_x+1, max_y+1, init_value=" ")
for tile in tiles:
    game_grid1.set_cell(tile[0], tile[1], tile_id[tile[2]])

game_grid1.print_grid()
block_count = 0
for i in range(0, game_grid1.width):
    for j in range(0, game_grid1.height):
        if game_grid1.get_cell(i, j) == tile_id[2]:
            block_count+=1
print(block_count)

#part 2
input_array = get_input("./inputs/day13input.txt")
cabinet = IntCode(input_array, [])
run_code = -1

game_grid = Grid(max_x+1, max_y+1, init_value=" ")
while(run_code != 0):
    run_code = cabinet.run_intcode()
    print(chr(27) + "[2J")
    block_count = 0

    i = 0
    tiles = [None]*(len(cabinet.output)//3)
    while i < len(cabinet.output):
        tiles[i//3]=[cabinet.output[i], cabinet.output[i+1], cabinet.output[i+2]]
        i+=3

    #this could be pulled outside the while loop but w/e
    max_x = 0
    max_y = 0
    for tile in tiles:
        max_x = max(tile[0], max_x)
        max_y = max(tile[1], max_y)


    #set paddle and ball locations
    for tile in tiles:
        if tile[0] == -1 and tile[1] == 0:
            score = tile[2]
        else:
            if tile[2] == 4:
                ball_xloc = tile[0]
            elif tile[2] == 3:
                paddle_xloc = tile[0]
            game_grid.set_cell(tile[0], tile[1], tile_id[tile[2]])

    game_grid.print_grid()
    print(score)
    if ball_xloc > paddle_xloc:
        cabinet.add_input([1])
    elif ball_xloc < paddle_xloc:
        cabinet.add_input([-1])
    else:
        cabinet.add_input([0])
    time.sleep(.01)