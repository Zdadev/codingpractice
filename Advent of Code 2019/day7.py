#this script is split up oddly because I needed to do some refactoring for part 2 and didnt 
#want to erase my work from part1
from day7Intcode import IntCode

def get_input(file_loc):
    input_file = open(file_loc, "r")
    # input_file = open("day7.test.txt", "r")
    split_array = input_file.read().split(",")
    for i in range(0, len(split_array)):
        split_array[i] = int(split_array[i])
    return split_array

def get_all_combos(array, val, combo_array):
    if(len(array)==0):
        combo_array.append(val)
    for i in range(0, len(array)):
        rec_val = val.copy()
        rec_array = array.copy()
        rec_val.append(rec_array.pop(i))
        get_all_combos(rec_array, rec_val, combo_array)
# ---------------------------------------------------------------------------------
#part1
def part1_run_amp(script, phase, input_instruction):
    script_input = [phase, input_instruction]
    intcode_script = IntCode(script.copy(), script_input)
    intcode_script.run_intcode()
    return intcode_script.output[0]
def part1_run_combo(script, combo_order):
    input_instruction = 0
    for i in combo_order:
        input_instruction = part1_run_amp(script, i, input_instruction)
    return input_instruction

split_array = get_input("./inputs/day7input.txt")
all_combos = []
get_all_combos([0, 1, 2, 3, 4], [], all_combos)
max_signal = part1_run_combo(split_array, all_combos[0])
for combo in all_combos:
    max_signal = max(part1_run_combo(split_array, combo), max_signal)
print("part1", max_signal)

#----------------------------------------------------------------------------------------
#part2
def init_amps(combo, script):
    ret = []
    for i in combo:
        amp = IntCode(script.copy(), [i])
        ret.append(amp)
    ret[0].add_input([0])
    return ret

def run_amps(amps):
    ret = 1
    while(ret != 0):
        for i in range(0, len(amps)):
            ret = amps[i].run_intcode()
            if(i == len(amps)-1):
                amps[0].add_input(amps[i].output)
            else:
                amps[i+1].add_input(amps[i].output)        

split_array = get_input("./inputs/day7input.txt")

all_combos = []
get_all_combos([5, 6, 7, 8, 9], [], all_combos)
max_signal = -1
for i in all_combos:
    amps = init_amps(i, split_array)
    run_amps(amps)
    max_signal = max(max_signal, amps[4].output[0])
print("part2", max_signal)

