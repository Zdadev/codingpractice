#https://adventofcode.com/2019/day/3
import sys
#returns a list of all the wire locations
#assumes start at origin
def wire_joint_locs(array):
    ret = [0]*(len(array)+1)
    lastx =  0
    lasty = 0

    ret[0] = [lastx, lasty]
    for i in range(0, len(array)):
        instruction = array[i]
        direction = instruction[0]
        wire_length = int(instruction[1:])
        if(direction =="D"):
            lasty = lasty - wire_length
        elif(direction == "U"):
            lasty = lasty + wire_length
        elif(direction == "L"):
            lastx = lastx - wire_length
        else:
            lastx = lastx + wire_length
        ret[i+1] = [lastx, lasty]
        
    return ret

#This was the stupidest way to go about this. 
# Now that I think about it, I should have just tested each section to see if there were any parts that intersected from them, and then returned the list of intersections. 
#At least I showed I know how to make a grid *facepalm*
def draw_panel(wire1, wire2):
    max_x = 0
    max_y = 0
    min_x = 0
    min_y = 0
    for i in wire1:
        max_x = max(i[0], max_x)
        min_x = min(i[0], min_x)
        max_y = max(i[1], max_y)
        min_y = min(i[1], min_y)
    for i in wire2:
        max_x = max(i[0], max_x)
        min_x = min(i[0], min_x)
        max_y = max(i[1], max_y)
        min_y = min(i[1], min_y)

    #origin is at 0 - min_x, 0 - min_y
    origin = [0-min_x, 0-min_y]
    panel = [0]*(max_y-min_y+1)
    for i in  range(max_y-min_y+1):
        panel[i] = [0]*(max_x - min_x +1)
    
    for i in range(1, len(wire1)):
        prev_loc = [wire1[i-1][0]-min_x, wire1[i-1][1]-min_y]
        curr_loc = [wire1[i][0]-min_x, wire1[i][1]-min_y]
        if(prev_loc[0] != curr_loc[0]):
            for j in range(min(prev_loc[0], curr_loc[0]), max(prev_loc[0], curr_loc[0])):
                panel[curr_loc[1]][j] = 1
        if(prev_loc[1] != curr_loc[1]):
            for j in range(min(prev_loc[1], curr_loc[1]), max(prev_loc[1], curr_loc[1])):
                panel[j][curr_loc[0]] = 1
    
    for i in range(1, len(wire2)):
        prev_loc = [wire2[i-1][0]-min_x, wire2[i-1][1]-min_y]
        curr_loc = [wire2[i][0]-min_x, wire2[i][1]-min_y]
        if(prev_loc[0] != curr_loc[0]):
            for j in range(min(prev_loc[0], curr_loc[0]), max(prev_loc[0], curr_loc[0])):
                panel[curr_loc[1]][j] = 2 if (panel[curr_loc[1]][j] == 0 or panel[curr_loc[1]][j] == 2) else 'x'
        if(prev_loc[1] != curr_loc[1]):
            for j in range(min(prev_loc[1], curr_loc[1]), max(prev_loc[1], curr_loc[1])):
                panel[j][curr_loc[0]] = 2 if (panel[j][curr_loc[0]] == 0 or panel[j][curr_loc[0]] == 2) else 'x'

    return [panel, origin]

def min_manhatten(panel, origin):
    min_dist = sys.maxsize
    for i in range(0, len(panel)):
        for j in  range(0, len(panel[i])):
            if(panel[i][j] == 'x' and (j != origin[0] or i != origin[1])):
                min_dist = min(min_dist, (abs(j - origin[0])+abs(i - origin[1])))
    return min_dist

def trace_wire(location, wire):
    def lies_between(location, start, end):
        if(start[0] != end[0] and start[1] == location[1]):
            if(location[0] >= min(start[0], end[0]) and location[0]<= max(start[0], end[0])):
                return True
        if(start[1] != end[1] and start[0] == location[0]):
            if(location[1] >= min(start[1], end[1]) and location[1]<= max(start[1], end[1])):
                return True
        return False
    dist = 0
    i = 1
    while (not lies_between(location, wire[i-1], wire[i])):
        dist += abs(wire[i][0]- wire[i-1][0]) + abs(wire[i][1]- wire[i-1][1])
        i+=1
    return dist + abs(location[0]- wire[i-1][0]) + abs(location[1]- wire[i-1][1])

def min_latentcy(panel, origin, wire1, wire2):
    #Get the crosses location relative to origin
    crosses = []
    for i in range(0, len(panel)):
        for j in  range(0, len(panel[i])):
            if(panel[i][j] == 'x' and (j != origin[0] or i != origin[1])):
                crosses.append([j-origin[0], i-origin[1]])
    #trace both wire until it reaches each of those locaions
    min_lat = sys.maxsize
    for i in crosses:
        min_lat = min(trace_wire(i, wire1) + trace_wire(i, wire2), min_lat)
    return min_lat

input_file = open("./inputs/day3input.txt")
lines = input_file.read().split()
wire1 = wire_joint_locs(lines[0].split(','))
wire2 = wire_joint_locs(lines[1].split(','))


panel = draw_panel(wire1, wire2)
print(min_manhatten(panel[0], panel[1]))
print(min_latentcy(panel[0], panel[1], wire1, wire2))