from day9Intcode import IntCode

def get_input(file_loc):
    input_file = open(file_loc, "r")
    # input_file = open("day7.test.txt", "r")
    split_array = input_file.read().split(",")
    for i in range(0, len(split_array)):
        split_array[i] = int(split_array[i])
    return split_array


input_array = get_input("./inputs/day9input.txt")
intcode_day9 = IntCode(input_array, [2])
intcode_day9.run_intcode()
print(intcode_day9.output)
