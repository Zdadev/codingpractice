from fractions import gcd
#This whole exercise ended up looking pretty sloppy. 
class V3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    
    def add(self, vec3):
        self.x += vec3.x
        self.y += vec3.y
        self.z += vec3.z

    @staticmethod
    def s_add(vec1, vec2):
        return V3(vec1.x + vec2.x, vec1.y+vec2.y, vec1.z+vec2.z)
 
    def sub(self, vec3):
        self.x -= vec3.x
        self.y -= vec3.y
        self.z -= vec3.z

    @staticmethod
    def s_sub(vec1, vec2):
        return V3(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z)

class Moon:
    def __init__(self, x, y, z):
        self.loc = V3(x, y, z)
        self.vel = V3(0,0,0)
        self.grav_change = V3(0, 0, 0)

    def calc_gravity(self, other_loc):
        if(other_loc.x > self.loc.x):
            self.grav_change.x += 1
        elif(other_loc.x < self.loc.x):
            self.grav_change.x -= 1
        
        if(other_loc.y > self.loc.y):
            self.grav_change.y += 1
        elif(other_loc.y < self.loc.y):
            self.grav_change.y -= 1
        
        if(other_loc.z > self.loc.z):
            self.grav_change.z += 1
        elif(other_loc.z < self.loc.z):
            self.grav_change.z -= 1
    
    def apply_gravity(self):
        self.vel.add(self.grav_change)
        self.grav_change = V3(0,0,0)
    def apply_velocity(self):
        self.loc.add(self.vel)
    def print_status(self):
        print(self.loc.x, self.loc.y, self.loc.z, self.vel.x, self.vel.y, self.vel.z)
    def get_energy(self):
        potential = abs(self.loc.x) + abs(self.loc.y) + abs(self.loc.z)
        kinetic = abs(self.vel.x) + abs(self.vel.y) + abs(self.vel.z)
        return potential * kinetic

def lcm(a, b):
    return (a*b)//gcd(a, b)

#test1
moons= [Moon(-1, 0, 2),Moon(2, -10, -7),Moon(4, -8, 8),Moon(3, 5, -1)]
#test2
# moons= [Moon(-8, -10, 0),Moon(5, 5, 10),Moon(2, -7, 3),Moon(9, -8, -3)]
moons = [Moon(-10, -13, 7),Moon(1, 2, 1),Moon(-15, -3, 13),Moon(3, 7, -4)]

steps = 1
for step in range(0, steps):
    print("step", step)
    #for each of the planets, apply the gravity done by each other planet
    for j in range(0, len(moons)):
        for k in range(0, len(moons)):
            if j != k:
                moons[j].calc_gravity(moons[k].loc)
    for moon in moons:
        moon.apply_gravity()
        moon.apply_velocity()

total_energy = 0
for moon in moons:
    total_energy += moon.get_energy()
print(total_energy)


moons = [Moon(-10, -13, 7),Moon(1, 2, 1),Moon(-15, -3, 13),Moon(3, 7, -4)]
moons2 = [Moon(-10, -13, 7),Moon(1, 2, 1),Moon(-15, -3, 13),Moon(3, 7, -4)]


x_loop = 0
y_loop = 0
z_loop = 0
i = 1
while(x_loop == 0 or y_loop == 0 or z_loop == 0):
    x_same = True
    y_same = True
    z_same = True
    for j in range(0, len(moons)):
        for k in range(0, len(moons)):
            if j != k:
                moons[j].calc_gravity(moons[k].loc)
    for j in range(0, len(moons)):
        moons[j].apply_gravity()
        moons[j].apply_velocity()
        #should probably have just made a test sameness function in moons. 
        if(moons[j].loc.x != moons2[j].loc.x or moons[j].vel.x != 0):
            x_same = False
        if(moons[j].loc.y != moons2[j].loc.y or moons[j].vel.y != 0):
            y_same = False
        if(moons[j].loc.z != moons2[j].loc.z or moons[j].vel.z != 0):
            z_same = False
    if(x_same and x_loop == 0):
        x_loop = i
    if(y_same and y_loop == 0):
        y_loop = i
    if(z_same and z_loop == 0):
        z_loop = i
    i+=1

print((lcm(x_loop, lcm(y_loop, z_loop))))