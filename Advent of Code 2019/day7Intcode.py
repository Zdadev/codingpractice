class IntCode:
    def __init__(self, array, script_input):
        self.array = array
        self.script_input = script_input
        self.input_counter = 0
        self.curr_address = 0
        self.waiting = True
        self.output = []
    def set_input(self, script_input):
        self.script_input = script_input
    def add_input(self, new_input):
        self.script_input += new_input

    def _interpret_instruction(self, val :int):
        val = int(val)
        ret = []
        if(val < 100):
            ret.append(val)
            return ret
        ret.append(val%100)
        val = int(val/100)
        while(val > 0):
            ret.append(val%10)
            val = int(val/10)
        return ret
    def _interpret_mode(self, loc, modes, spacing):
        if(len(modes)<spacing or modes[spacing-1] != 1):
            return self.array[self.array[loc+spacing]]
        else:
            return self.array[loc+spacing]
    def _interpert_ret(self, loc, spacing, params):
        if(self.array[loc+spacing] == loc):
            return 0
        else:
            return params

    def _intcode1_add(self, loc, modes):
        param_1 = self._interpret_mode(loc, modes, 1)
        param_2 = self._interpret_mode(loc, modes, 2)
        ret = self._interpert_ret(loc, 3, 4)
        self.array[self.array[loc+3]] = param_1 + param_2
        return ret
    def _intcode2_mult(self, loc, modes):
        param_1 = self._interpret_mode(loc, modes, 1)
        param_2 = self._interpret_mode(loc, modes, 2)
        ret = self._interpert_ret(loc, 3, 4)
        self.array[self.array[loc+3]] = param_1 * param_2
        return ret
    def _intcode3_input(self, loc, modes):
        if(self.input_counter >= len(self.script_input)):
            self.waiting = True
            return 0
        ret = self._interpert_ret(loc, 1, 2)
        self.array[self.array[loc+1]] = self.script_input[self.input_counter]
        self.input_counter += 1
        return ret
    def _intcode4_output(self, loc, modes):
        output = self._interpret_mode(loc, modes, 1)
        self.output.append(output)
        return 2
    def _intcode5_jump_if_true(self, loc, modes):
        check_if_true = self._interpret_mode(loc, modes, 1)
        jump_to = self._interpret_mode(loc, modes, 2)   

        if(check_if_true != 0):
            return (jump_to - loc)
        else:
            return 3
    def _intcode6_jump_if_false(self, loc, modes):
        check_if_true = self._interpret_mode(loc, modes, 1)
        jump_to = self._interpret_mode(loc, modes, 2)   
        if(check_if_true == 0):
            return (jump_to - loc)
        else:
            return 3
    def _intcode7_less_than(self, loc, modes):
        param_1 = self._interpret_mode(loc, modes, 1)
        param_2 = self._interpret_mode(loc, modes, 2)   
        ret = self._interpert_ret(loc, 3, 4)

        if (param_1 < param_2):
            self.array[self.array[loc+3]] = 1
        else:
            self.array[self.array[loc+3]] = 0
        return ret
    def _intcode8_equals(self, loc, modes):
        param_1 = self._interpret_mode(loc, modes, 1)
        param_2 = self._interpret_mode(loc, modes, 2)   
        ret = self._interpert_ret(loc, 3, 4)

        if (param_1 == param_2):
            self.array[self.array[loc+3]] = 1
        else:
            self.array[self.array[loc+3]] = 0
        return ret

    # 0 indicates intcode halted
    # 1 indicates intcode is waiting for input
    # 2 indicates invalid optcode was called
    # 3 indicates the script reached the end of memory without halting
    def run_intcode(self):
        int_switch={
            1: self._intcode1_add,
            2: self._intcode2_mult,
            3: self._intcode3_input,
            4: self._intcode4_output,
            5: self._intcode5_jump_if_true,
            6: self._intcode6_jump_if_false,
            7: self._intcode7_less_than,
            8: self._intcode8_equals
        }
        self.output = []
        self.waiting = False
        while(self.curr_address < len(self.array) and not self.waiting):
            instruction = self._interpret_instruction(self.array[self.curr_address])
            if(instruction[0] == 99):
                return 0
            if(not instruction[0] in int_switch):
                print("invalid opt_code", self.array[self.curr_address])
                return 2
            self.curr_address += int_switch[instruction[0]](self.curr_address, instruction[1:])
        if self.waiting:
            return 1
        else:
            print("Reached end of script without halting")
            return 3
