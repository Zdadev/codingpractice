#https://adventofcode.com/2019/day/2
#cleaned up and packaged as object in day7intcode.py

def exercise1(array):
    i = 0
    while(i < len(array)):
        if(array[i] == 1):
            array[array[i+3]] = array[array[i+1]] + array[array[i+2]]
        elif(array[i] == 2):
            array[array[i+3]] = array[array[i+1]] * array[array[i+2]]
        elif(array[i] == 99):
            i = len(array)
        else:
            raise Exception("Non-valid opcode")
        i += 4

    return array[0]

def check_output(mem, i, j, val):
    mem[1] = i
    mem[2] = j
    if(exercise1(mem) == val):
        return True
    return False

def find_output(mem, val):
    tmp_mem = mem.copy()
    for i in range(0, 99):
        for j in range(0, 99):
            try:
                valid_output = check_output(tmp_mem, i, j, val)
            except:
                valid_output = False
            if valid_output:
                return i + j * 100
            print(i, j)
            tmp_mem = mem.copy()

input_file = open("./inputs/day2input.txt", "r")
split_array = input_file.read().split(",")
for i in range(0, len(split_array)):
    split_array[i] = int(split_array[i])



print(find_output(split_array, 19690720))