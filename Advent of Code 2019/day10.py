#pretty sure we can just count unique fraction values for each asteroid?

# input_file = open("./inputs/day10input.txt", "r")
input_file = open("day10test.txt", "r")
split_array = input_file.read().split()
for i in range(0, len(split_array)):
    split_array[i] = list(split_array[i])

def get_slope(x_loc, y_loc, x, y):
    if(x_loc == x):
        return float("inf") if y_loc > y else float("-inf")
    if(y_loc == y):
        return 0
    #y coords are upside down
    return (y_loc-y)/(x-x_loc)

#Part 2 made this function useless since we can accomplish the same thing in the same time by using get_visible and adding them together
def count_unique_frac(x_loc, y_loc, asteroid_map):
    left_fractions = set()
    right_fractions = set()
    for y in range(0, len(asteroid_map)):
        for x in range(0, len(asteroid_map[y])):
            if((x_loc != x or y_loc != y) and asteroid_map[y][x] == '#'):
                asteroid_slope = get_slope(x_loc, y_loc, x, y)
                if(x_loc<x):
                    left_fractions.add(asteroid_slope)
                else:
                    right_fractions.add(asteroid_slope)
    return len(right_fractions) + len(left_fractions)

#returns true if coordinate set 2 is closer (manhatten)
def check_closer(x_loc, y_loc, coord1, coord2):
    return abs((x_loc - coord1[0]) + (y_loc - coord1[1])) > abs((x_loc - coord2[0]) + (y_loc - coord2[1]))

def get_visible(x_loc, y_loc, asteroid_map):
    left_dict = {}
    right_dict = {}
    for y in range(0, len(asteroid_map)):
        for x in range(0, len(asteroid_map[y])):
            if((x_loc != x or y_loc != y) and asteroid_map[y][x] == '#'):
                asteroid_slope = get_slope(x_loc, y_loc, x, y)
                #this is ugly nested if statements :/
                if(x_loc>x):
                    if((not asteroid_slope in left_dict) or check_closer(x_loc, y_loc, left_dict[asteroid_slope], [x, y])):
                        left_dict[asteroid_slope] = [x, y]
                else:
                    if((not asteroid_slope in right_dict) or check_closer(x_loc, y_loc, right_dict[asteroid_slope], [x, y])):
                        right_dict[asteroid_slope] = [x, y]
    return [right_dict, left_dict]
    


max_asteroids = 0

for y in range(0, len(split_array)):
    for x in range(0, len(split_array[y])):
        if(split_array[y][x] == '#'):
            unique_frac = count_unique_frac(x, y, split_array)
            if(unique_frac > max_asteroids):
                max_x = x
                max_y = y
                max_asteroids = unique_frac
#Part 2
to_destroy = 200
destroyed = 0
while(destroyed < to_destroy):
    in_path = get_visible(max_x, max_y, split_array)
    right_path = list(in_path[0].keys())
    right_path.sort(reverse=True)
    left_path = list(in_path[1].keys())
    left_path.sort(reverse=True)

    for asteroid in right_path:
        destroyed += 1
        if destroyed == to_destroy:
            print("TO DESTROY:", in_path[0][asteroid])
        split_array[in_path[0][asteroid][1]][in_path[0][asteroid][0]] = '.'
    for asteroid in left_path:
        destroyed += 1
        if destroyed == to_destroy:
            print("TODESTROY:", in_path[1][asteroid])
        split_array[in_path[1][asteroid][1]][in_path[1][asteroid][0]] = '.'