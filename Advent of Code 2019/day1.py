def exercise_one():
    ret = 0
    input_file_1 = open("./inputs/day1input.txt", "r")
    for mass in input_file_1.read().split():
        ret+= (int(int(mass)/3)-2)
    return ret

def exercise_two():
    ret = 0

    input_file_1 = open("./inputs/day1input.txt", "r")
    for mass in input_file_1.read().split():
        fuel_needed = (int(int(mass)/3)-2)
        while(fuel_needed > 0):
            ret += fuel_needed
            fuel_needed = (int(int(fuel_needed)/3)-2)
    return ret

print(exercise_one())
print(exercise_two())