#https://adventofcode.com/2019/day/6
class Orbiter:
    def __init__(self, orbiter_name):
        self.name = orbiter_name
        self.orbits = None
    
    def add_orbit(self, orbit):
        self.orbits = orbit

    def total_orbits(self):
        if self.orbits == None:
            return 0
        total = self.orbits.total_orbits() + 1
        return total
    
    def all_orbits(self):
        if self.orbits == None:
            return list()
        all_orbits = self.orbits.all_orbits()
        all_orbits.append(self.name)
        return all_orbits
    
    #returns 0 if already orbiting 'name'
    def steps_to(self, name):
        if self.name == name:
            return -1
        return self.orbits.steps_to(name) + 1



#get input, split by line, split lines by )
input_file = open("./inputs/day6input.txt", "r")
input_array = input_file.read().split()
for i in range(0, len(input_array)):
    input_array[i] = input_array[i].split(")")

satellite_dict = {}
for i in input_array:
    if not i[0] in satellite_dict:
        satellite_dict[i[0]] = Orbiter(i[0])
    if not i[1] in satellite_dict:
        satellite_dict[i[1]] = Orbiter(i[1])
    satellite_dict[i[1]].add_orbit(satellite_dict[i[0]])

count = 0
for key in satellite_dict:
    count += satellite_dict[key].total_orbits()
print(count)

#part2
#could be done with djistras by making the tree bidirectional,
#but going to solve it in the current framework (and not very optiized)

#get all orbits of YOU and SAN
you_orbits = satellite_dict["YOU"].all_orbits()
san_orbits = satellite_dict["SAN"].all_orbits()


i = 1
while i < len(you_orbits)+1:
    if you_orbits[-i] in san_orbits:
        print("closest orbit", you_orbits[-i])
        break
    i+=1
print(satellite_dict["YOU"].steps_to(you_orbits[-i]) + satellite_dict["SAN"].steps_to(you_orbits[-i]))