#https://adventofcode.com/2019/day/5
#cleaned up and packaged as object in day7intcode.py
def interpret_instruction(val :int):
    val = int(val)
    ret = []
    if(val < 100):
        ret.append(val)
        return ret
    ret.append(val%100)
    val = int(val/100)
    while(val > 0):
        ret.append(val%10)
        val = int(val/10)
    return ret
            
def intcode1_add(array, loc, modes, intcode_input):
    #set param 1
    if(len(modes)<1 or modes[0] != 1):
        param_1 = array[array[loc+1]]
    else:
        param_1 = array[loc+1]
    #set param 2
    if(len(modes)<2 or modes[1] != 1):
        param_2 = array[array[loc+2]]
    else:
        param_2 = array[loc+2]

    if(array[loc+3] == loc):
        ret = 0
    else:
        ret = 4

    array[array[loc+3]] = param_1 + param_2
    return ret

def intcode2_mult(array, loc, modes, intcode_input):
    #set param 1
    if(len(modes)<1 or modes[0] != 1):
        param_1 = array[array[loc+1]]
    else:
        param_1 = array[loc+1]
    #set param 2
    if(len(modes)<2 or modes[1] != 1):
        param_2 = array[array[loc+2]]
    else:
        param_2 = array[loc+2]
    
    if(array[loc+3] == loc):
        ret = 0
    else:
        ret = 4

    array[array[loc+3]] = param_1 * param_2
    return ret

def intcode3_input(array, loc, modes, intcode_input):
    if(array[loc+1]==loc):
        ret = 0
    else:
         ret = 2
    array[array[loc+1]] = intcode_input
    return ret
def intcode4_output(array, loc, modes, intcode_input):
    if(len(modes)<1 or modes[0] != 1):
        print_output = array[array[loc+1]]
    else:
        print_output = array[loc+1]
    print(print_output)
    return 2
def intcode5_jump_if_true(array, loc, modes, intcode_input):
    # might want to make this pattern a function since we are using it so much?
    if(len(modes)<1 or modes[0] != 1):
        check_if_true = array[array[loc+1]]
    else:
        check_if_true = array[loc+1]

    if(len(modes)<2 or modes[1] != 1):
        jump_to = array[array[loc+2]]
    else:
        jump_to = array[loc+2]    

    if(check_if_true != 0):
        # print("jump")
        return (jump_to - loc)
    else:
        return 3
def intcode6_jump_if_false(array, loc, modes, intcode_input):
    # might want to make this pattern a function since we are using it so much?
    print(modes, array[loc+1])
    if(len(modes)<1 or modes[0] != 1):
        check_if_true = array[array[loc+1]]
    else:
        check_if_true = array[loc+1]

    if(len(modes)<2 or modes[1] != 1):
        jump_to = array[array[loc+2]]
    else:
        jump_to = array[loc+2]    

    if(check_if_true == 0):
        # print("jump")
        return (jump_to - loc)
    else:
        return 3
def intcode7_less_than(array, loc, modes, intcode_input):
    #set param 1
    if(len(modes)<1 or modes[0] != 1):
        param_1 = array[array[loc+1]]
    else:
        param_1 = array[loc+1]
    #set param 2
    if(len(modes)<2 or modes[1] != 1):
        param_2 = array[array[loc+2]]
    else:
        param_2 = array[loc+2]

    if(array[loc+3] == loc):
        ret = 0
    else:
        ret = 4

    if (param_1 < param_2):
        array[array[loc+3]] = 1
    else:
        array[array[loc+3]] = 0
    return ret
def intcode8_equals(array, loc, modes, intcode_input):
    #set param 1
    if(len(modes)<1 or modes[0] != 1):
        param_1 = array[array[loc+1]]
    else:
        param_1 = array[loc+1]
    #set param 2
    if(len(modes)<2 or modes[1] != 1):
        param_2 = array[array[loc+2]]
    else:
        param_2 = array[loc+2]

    if(array[loc+3] == loc):
        ret = 0
    else:
        ret = 4

    if (param_1 == param_2):
        array[array[loc+3]] = 1
    else:
        array[array[loc+3]] = 0
    return ret


def intcode(array, intcode_input):
    int_switch={
        1: intcode1_add,
        2: intcode2_mult,
        3: intcode3_input,
        4: intcode4_output,
        5: intcode5_jump_if_true,
        6: intcode6_jump_if_false,
        7: intcode7_less_than,
        8: intcode8_equals
    }
    i = 0
    while(i< len(array)):
        # print(array[i], array)
        instruction = interpret_instruction(array[i])
        if(instruction[0] == 99):
            print("program halted")
            return
        if(not instruction[0] in int_switch):
            print("invalid opt_code", array[i])
            return
        i += int_switch[instruction[0]](array, i, instruction[1:], intcode_input)

input_file = open("./inputs/day5input.txt", "r")
split_array = input_file.read().split(",")
for i in range(0, len(split_array)):
    split_array[i] = int(split_array[i])

intcode(split_array, 5)
# print(interpret_instruction(3))
