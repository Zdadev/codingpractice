class Grid:
    def __init__(self, x_size, y_size, x_offset = 0, y_offset = 0, init_value = None):
        self.grid_holder = [None]*y_size
        for i in range(0, y_size):
            self.grid_holder[i] = [init_value]*x_size
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.width = x_size
        self.height = y_size

    def get_cell(self, x, y):
        return self.grid_holder[y][x]
    def set_cell(self, x, y, val):
        self.grid_holder[y][x] = val
    def print_grid(self):
        print_string = ""
        for row in self.grid_holder:
            print_string += " ".join(row) + "\n"
        print(print_string)
    