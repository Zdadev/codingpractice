# https://www.reddit.com/r/dailyprogrammer/comments/bazy5j/20190408_challenge_377_easy_axisaligned_crate/?sort=top
library(igraph)

vector_permutations <- function(vector){
    n = length(vector)
    perms <<- vector("list", length=factorial(n))
    counter <<- 1
    depth = 0
    permutation_recursion <- function(building_array, left, depth){
        if(length(left) == 1){
            perms[[counter]] <<- c(building_array, left)
            counter <<- counter+1
            return(TRUE) 
        }
        for(i in c(1:length(left))){
            new_array <- c(building_array, c(left[i]))
            new_left = left[-i]
            permutation_recursion(new_array, new_left, depth+1)
        }
        return(TRUE)
    }

    permutation_recursion(c(), c(1:n), depth)
    ret <- vector("list", length=factorial(n))
    for(i in c(1:length(perms))){
        ret[[i]] = vector[perms[[i]]]
    }
    return(ret)
}

fit1 <- function(X, Y, x, y){
    return(as.integer(X/x) * as.integer(Y/y))
}
fit2 <- function(X, Y, x, y){
    return(max(fit1(X, Y, x, y), fit1(X, Y, y, x)))
}
#niave fitn: generate all permutations, find the best one
fitn <- function(outer, inner){
    fitn_helper <-function(outer, inner){
        ret <- 1
        for(i in c(1:length(inner))){
            ret <- ret*as.integer(outer[i]/inner[i])
        }
        return(ret)
    }
    inner_permutations <- vector_permutations(inner)
    max <- 0
    for(i in c(1:length(inner_permutations))){
        value = fitn_helper(outer,inner_permutations[[i]])
        if(value > max){
            max <- value
        }
    }
    return(max)
}
#non-naive, using maximum matching algorithm
fitnO <- function(outer, inner){
    bi_graph <- make_empty_graph()
    for(i in c(1:length(outer))){
        bi_graph <-add_vertices(bi_graph, 1, type=TRUE, dim = outer[i])
    }
    for(i in c(1:length(inner))){
        bi_graph <-add_vertices(bi_graph, 1, type=FALSE, dim = inner[i])
    }
    for(i in c(1:length(outer))){
        for(j in c(1:length(inner))){
            bi_graph <- add_edges(bi_graph, c(i, length(outer)+j), weight=(log10(as.integer(outer[i]/inner[j]))))
        }
    }
    return(10**(max_bipartite_match(bi_graph)[["matching_weight"]]))
}

# print(fit1(25, 18, 6, 5))
# print(fit2(25, 18, 6, 5))
# print(c(1:3)[-2])
# fitn(c(12,34,56),c(7,8,9))
# fitn(c(123, 456, 789), c(10, 11, 12))
fitnO(c(123, 456, 789), c(10, 11, 12))
# fitn(c(123, 456, 789, 1011, 1213, 1415), c(16, 17, 18, 19, 20, 21))
fitnO(c(123, 456, 789, 1011, 1213, 1415), c(16, 17, 18, 19, 20, 21))
