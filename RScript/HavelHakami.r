#https://www.reddit.com/r/dailyprogrammer/comments/bqy1cf/20190520_challenge_378_easy_the_havelhakimi/
havel_hakami <- function(answers){
    answers <- sort(Filter(function(x){ifelse(x!=0, TRUE, FALSE)}, answers), decreasing=TRUE)
    while(length(answers) > 1){
        curr <- answers[1]
        answers <- answers[2:length(answers)]
        if(curr>length(answers)){
            return(FALSE)
        }
        for(i in c(1:curr)){
            answers[i] <- answers[i]-1
        }
        answers <- sort(Filter(function(x){ifelse(x!=0, TRUE, FALSE)}, answers), decreasing=TRUE)
    }
    return(ifelse(length(answers)==1,FALSE,TRUE))
}

print(havel_hakami(c(5,3,0,2,6,2,0,7,2,5)))
print(havel_hakami(c(4,2,0,1,5,0)))
print(havel_hakami(c(3,1,2,3,1,0)))
print(havel_hakami(c(16, 9, 9, 15, 9, 7, 9, 11, 17, 11, 4, 9, 12, 14, 14, 12, 17, 0, 3, 16)))
print(havel_hakami(c(14, 10, 17, 13, 4, 8, 6, 7, 13, 13, 17, 18, 8, 17, 2, 14, 6, 4, 7, 12)))
print(havel_hakami(c(15, 18, 6, 13, 12, 4, 4, 14, 1, 6, 18, 2, 6, 16, 0, 9, 10, 7, 12, 3)))# => false
print(havel_hakami(c(6, 0, 10, 10, 10, 5, 8, 3, 0, 14, 16, 2, 13, 1, 2, 13, 6, 15, 5, 1))) #=> false
print(havel_hakami(c(2, 2, 0)))# => false
print(havel_hakami(c(3, 2, 1)))# => false
print(havel_hakami(c(1, 1)))# => true
print(havel_hakami(c(1)))# => false
print(havel_hakami(c()))# => true