
#9/6/2019 Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

# For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17
possible_sum <- function(int_array, target){
    print(int_array)
    print(target)
    for(i in c(1:(length(int_array)-1))){
        for(j in c((i+1):length(int_array))){
            if((int_array[i]+int_array[j])==target){
                return(TRUE)
            }
        }
    }
    return(FALSE)
#A faster solution would be to make an empty hash table. Go through the list, checking if target-list[i] has a collision, then add that number to the hash. R doesn't have a native hashmap class though.
}

# possible_sum(c(10,15,3,7),17)
# possible_sum(c(10,15,3,7),19)

# Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

# For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].

#Follow-up: what if you can't use division?
#My answer: make an array of logs and sum the remainders


make_product_exception_array <- function(array){
    ret = vector(length=length(array))
    base_product = prod(array)
    ret = sapply(array, function(x){return(base_product/x)})
    return(ret)
}
make_product_exception_array(c(1,2,3,4,5))