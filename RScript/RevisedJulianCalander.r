# https://www.reddit.com/r/dailyprogrammer/comments/b0nuoh/20190313_challenge_376_intermediate_the_revised/
options(scipen=20)

is_leap_year <- function(year){
    if(year%%4 != 0){
        return(FALSE)
    }
    if(year%%100 == 0 && year%%900 != 200 && year%%900 != 600){
        return(FALSE)
    }
    return(TRUE)
}
is_cycle_year <- function(year){
    if((year-200)%%900 == 0){
        return(TRUE)
    }
    return(FALSE)
}
brute_num_leap_years <- function(start, end){
    count <- 0
    curr_year <- start
    while(curr_year != end){
        if(is_leap_year(curr_year)){
            count <- count+1
        }
        curr_year <- curr_year + 1
    }
    return(count)
}
num_leap_years <- function(start, end){
    if(end-start <=900){
        return(brute_num_leap_years(start,end))
    }
    count <- 0
    curr_year <- start
    #Count years until first cycle 
    while(!is_cycle_year(curr_year)){
        if(is_leap_year(curr_year)){
            count <- count+1
        }
        curr_year <- curr_year + 1
    }
    years_left <- end - curr_year
    remainder <- years_left %% 900
    #Find how many cycles are between the start of that cycle and the end start and end 900 until that number would go over the last one
    #218 leap years per 900 year cycle
    count <- count + round(years_left/900)*218
    #count years until end
    count <- count + num_leap_years(200, remainder+200)
    return(count)
}
brute_num_leap_years(2016,2017) #1
brute_num_leap_years(2019,2020) #0
brute_num_leap_years(1900,1901) #0
brute_num_leap_years(1234,5678) #1077
brute_num_leap_years(200,1100) #1077
num_leap_years(123456789101112, 1314151617181920) #288412747246240