# https:,,www.reddit.com,r,dailyprogrammer,comments,7z8hrm,20180221_challenge_352_intermediate_7_wonders,
library(igraph)
#Implemented without brute force, and with non-specific symbols
check_resources <- function(cards, cost){
    bi_graph <- make_empty_graph()
    #make a vertex for each of the cost
    for(i in c(1:length(cost))){
        bi_graph <-add_vertices(bi_graph, 1, type=TRUE, cost = cost[i])
    }
    #for each of the cards
    for(i in c(1:length(cards))){
        #make a vertex for that card
        bi_graph <-add_vertices(bi_graph, 1, type=FALSE, cost = cost[i])
        #Add edges to matching resource values
        for(j in c(1:length(cards[[i]]))){
            if(is.element(cards[[i]][j], cost)){
                cost_locations <- which(cards[[i]][j] == cost)
                for(k in cost_locations){
                    bi_graph <- add_edges(bi_graph, c(i+length(cost), k))
                }
            }
        }
    }
    #check to see if you can match the graph
    return(max_bipartite_match(bi_graph)[["matching_size"]] == length(cost))
}
check_resources(list(c('W','B','S','O'), c('W'), c('S', 'B'), c('S')), c('W','W','S','S'))
check_resources(list(c('W','B','S','O'), c('S','O'), c('W','S'), c('W','B'), c('W','B'), c('W'), c('B')), c('W','W','B','S','S','O','O'))
check_resources(list(c('A','B','D','E'), c('A','B','E','F','G'), c('A','D'), c('A','D','E'), c('A','D','E'), c('B','C','D','G'), c('B','C','E'), c('B','C','E','F'), 
c('B','C','E','F'), c('B','D','E'), c('B','D','E'), c('B','E','F'), c('C','D','F'), c('C','E'), c('C','E','F','G'), c('C','F'), c('C','F'), c('D','E','F','G'), 
c('D','F'), c('E','G')),c('A','A','B','C','C','C','C','C','C','D','D','D','E','E','E','E','F','F','G','G'))